<?php
/**
 * @file
 *
 * Defines a custom field for constructing a custom buttonlink.
 */


/**
 * Implements hook_field_info().
 *
 * Here we tell Drupal about our custom fields. In this case
 * we only have one. Its machine-readable name is 'buttonlink_maker_ buttonlink'
 * because the convention is 'modulename_fieldname'.
 *
 * We also define the machine names of the widget and the formatter.
 */
function buttonlink_maker_field_info() {
  return array(
    'buttonlink_maker_buttonlink' => array(
      'label' => t('Custom ButtonLink'),
      'description' => t('Custom ButtonLink Field'),
      'default_widget' => 'buttonlink_maker_buttonlink_widget',
      'default_formatter' => 'buttonlink_maker_buttonlink_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 *
 * Here we tell Drupal about our custom widgets. In this
 * case we only have one. As with buttonlink_maker_field_formatter_info(),
 * we tell Drupal which fields our widget works with (in this case, just
 * 'buttonlink_maker_buttonlink').
 */
function buttonlink_maker_field_widget_info() {
  return array(
    'buttonlink_maker_buttonlink_widget' => array(
      'label' => t('Default'),
      'field types' => array('buttonlink_maker_buttonlink'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * http://api.drupal.org/api/drupal/modules--field--field.api.php/function/hook_field_widget_form/7
 * 
 * Here we define a form element that the user inputs data
 * into. If we have a complex custom field, we can have many sub-elements
 * organized into fieldsets.
 */
function buttonlink_maker_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // $field_name is the name of the field instance. This is the
  // name that the administrator gives the field instance when
  // attaching it to an entity.
  $field_name = $instance['field_name'];

  // Is this field instance required?
  // We can use this $required value to make sub-elements of the
  // widget form required.
  $required = $element['#required'];

  // $item is where the current saved values are stored
  $item =& $items[$delta];

  // $element is already populated with #title, #description, #delta,
  // #required, #field_name, etc.
  //
  // In this example, $element is a fieldset, but it could be any element
  // type (textfield, checkbox, etc.)
  $element += array(
    '#type' => 'fieldset',
  );

  // Array keys in $element correspond roughly
  // to array keys in $item, which correspond
  // roughly to columns in the database table.
  $element['cssclass'] = array(
    '#title' => t('CSS Class'),
    '#type' => 'textfield',
    '#required' => $required,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['cssclass']) ? $item['cssclass'] : '',
  );

  $element['action'] = array(
    '#title' => t('Action'),
    '#type' => 'textfield',
    '#required' => $required,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['action']) ? $item['action'] : '',
  );

  $element['text'] = array(
    '#title' => t('Text'),
    '#type' => 'textfield',
    '#required' => $required,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['text']) ? $item['text'] : '',
  );

  return $element;
}



/**
 * Implements hook_field_is_empty().
 *
 * buttonlink_maker_field_is_empty() lets Drupal know whether or not to process input
 * at all; if the field is empty, it won't bother validating
 * or saving the values.
 */
function buttonlink_maker_field_is_empty($item, $field) {
  $has_stuff = FALSE;
  if (!empty($item['cssclass'])) {
    $has_stuff = TRUE;
  }
  if (!empty($item['action'])) {
    $has_stuff = TRUE;
  }
  if (!empty($item['text'])) {
    $has_stuff = TRUE;
  }
  return !$has_stuff;
}



/**
 * Implements hook_field_widget_error().
 *
 * buttonlink_maker_field_widget_error() takes the errors set in buttonlink_maker_field_validate()
 * and tells Drupal how to notify the user of the error. For example, this implementation
 * calls form_error() with each element passed in the $error['error_elements'] array, so
 * we can identify a problem with several elements in our field (e.g. tofu and sweet potato).
 */
function buttonlink_maker_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
  // Handle the error that was set in buttonlink_maker_field_validate()
  case 'buttonlink_maker_invalid_full':
    form_error($element, $error['message']);
    break;
  }
}



/**
 * Implements hook_field_formatter_info().
 *
 * Here we tell Drupal about our custom formatters. In this
 * case we only have one. Note that our formatter works on
 * the field type 'buttonlink_maker_buttonlink', which is
 * the machine-readable name we defined in buttonlink_maker_field_info().
 */
function buttonlink_maker_field_formatter_info() {
  return array(
    'buttonlink_maker_buttonlink_formatter' => array(
      'label' => t('Default'),
      'field types' => array('buttonlink_maker_buttonlink'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 * This function gives Drupal a structured array, which
 * defines how the data stored in the field should be rendered.
 */
function buttonlink_maker_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'buttonlink_maker_buttonlink_formatter':
      $format_field = 'buttonlink_maker_format_default_field';
      break;
  }
  foreach ($items as $delta => $item) {
    $element[$delta] = $format_field($item);
  }

  return $element;
}

/**
 * Helper to render a single formatted entry. Returns
 * a structured array that will display the data stored in
 * $item.
 */
function buttonlink_maker_format_default_field($item) {
  
  
  $element = array(
    '#type' => 'link',
    '#attributes' => array( 
      'class' => array( 'btn', $item['cssclass'] ) ,
    ),
    '#title' => $item['text'],
    '#href' => $item['action'],
  );
  

  return $element;
}



/**
 * Implements hook_field_presave().
 */
function buttonlink_maker_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  if ($field['type'] == 'buttonlink_maker_ buttonlink') {
    foreach ($items as $delta => &$item) {
      $new_item = array();
      $new_item['cssclass'] = $item['cssclass'];
      $new_item['action'] = $item['action'];
      $new_item['text'] = $item['text'];

      $item = $new_item;
    }
  }
}

/**
 * Implements hook_field_load().
 */
function buttonlink_maker_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => &$item) {
      $new_item = array();
      $new_item['cssclass'] = $item['cssclass'];
      $new_item['action'] = $item['action'];
      $new_item['text'] = $item['text'];

      $item = $new_item;
    }
  }
}

function buttonlink_make_field_display_alter(&$display, $context) {
  print_r($display);
}
