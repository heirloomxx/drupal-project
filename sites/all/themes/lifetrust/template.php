<?php 

/** 
* Override or insert variables into the page template. 
*/ 
function lifetrust_preprocess_page(&$vars) {
  // Get super menu links
  $vars['super_menu'] = menu_navigation_links('menu-super-menu');
  // Get footer menu links
  $vars['footer_menu'] = menu_navigation_links('menu-footer-menu');
  // Get sidebar menu links
  $vars['sidebar_menu'] = menu_navigation_links('menu-sidebar-menu');
  
  if(isset($vars['node']->type)) { 
  	$vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type; } 
}

?>