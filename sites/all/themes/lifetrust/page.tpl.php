<div id="container">
    <!--header-->
    <header>
        <!--super menu-->
        <?php if (theme_get_setting('super_menu','lifetrust')): ?>
            <div class="supernav">
                <div class="frame clearfix">
                    <?php print theme('links__menu-super-menu', array('links' => $super_menu)); ?>
                </div>
            </div>
        <?php endif; ?>
        <!--EOF:super menu-->
        <!--logo and main menu-->
        <nav class="mainnav">
            <div class="frame clearfix">
                <?php if ($logo): ?>
                    <strong class="logo">
                        <a href="<?php print check_url($front_page); ?>" title="<?php print t('Lifetrust'); ?>">
                            <img src="<?php print $logo; ?>" alt="<?php print t('Lifetrust'); ?>" />
                        </a>
                    </strong>
                    <?php endif; ?>
                <?php print theme('links__system_main_menu', array('links' => $main_menu)); ?>
            </div>
        </nav>  
        <!--EOF:logo and main menu--> 
        <div class="mainbanner">
            <div class="shadow">
                <div class="bannerholder clearfix">
                    <?php print render($page['banner']);?>
                </div>
             </div>
        </div>     
    </header>
    <!--EOF:header-->
        <article>
        <div class="shadow">
            <div class="contentholder">
                <div class="shadowcontentholder clearfix">
                    <div class="content">
                        <div class="contactbox">
                            <div class="imgsection"><img src="sites/all/themes/lifetrust/images/contactman.png" alt="" /></div>
                            <div class="contacts">
                                <div class="title">Call us today!</div>
                                <dl>
                                    <dt>Tel:</dt>
                                    <dd>212.653.0840</dd>
                                    <dt>Fax:</dt>
                                    <dd>212.653.0844</dd>
                                </dl>
                                <div class="btnsection"><a href="#link" class="btn contactus">contact us</a></div>
                            </div>
                        </div>
                        <div class="ohidden">
                            <ul class="services">
                                <?php print render($page['content']); ?>
                            </ul>
                            <div class="service"></div>
                        </div>
                    </div>
                    <div class="sidebar">
                        <?php print theme('links__menu-sidebar-menu', array('links' => $sidebar_menu)); ?>
                    </div>
                     <?php print render($page['sidebar_first']); ?>
                </div>
            </div>
        </div>
    </article>              
    <footer>
        <div class="frame">
            <?php if (theme_get_setting('footer_menu','lifetrust')): ?>
                <div class="row clearfix">
                    <?php print theme('links__menu-footer-menu', array('links' => $footer_menu)); ?>
                    <div class="privacy"><a href="#link">Privacy Policy</a></div>
                </div>
            <?php endif; ?>
            <div class="row clearfix">
                <div class="copy">&copy; 2010 LifeTrust. All Rights Reserved.</div>
                <div class="by"><a href="http://www.bluefountainmedia.com" target="_blank">Website Design</a> by <a href="http://www.bluefountainmedia.com/blog" target="_blank">Blue Fountain Media</a></div>
            </div>
        </div>
    </footer>
</div>
<!--EOF:container-->